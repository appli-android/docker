let session = require('cookie-session'); // Charge le middleware de sessions
let bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
let mongoose = require('mongoose');
let express = require('express');
let pug = require('pug');
let csv = require('fast-csv'); // CSV
const fs = require('fs'); // Permet d'accéder aux système de fichiers
const ws = fs.createWriteStream('todos.csv');
const flash = require('connect-flash');


let urlencodedParser = bodyParser.urlencoded({ extended: false });
let app = express();
const port = 8080;

// Connection au cluster Atlas
mongoose.connect("mongodb+srv://admin:admin@cluster0.fayereu.mongodb.net/todolist", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

// Définition de la manière dont les données seront stockées dans la db
const todoSchema = new mongoose.Schema({
    todo: String,
    category: String
});

// Définis la collection Tasks de la db et permet d'intégrer un CRUD
const Todos = mongoose.model("Todos", todoSchema);

// On utilise les sessions
app.use(session({ secret: 'todotopsecret' }))

    // Initialisation : creation d'une liste vide en session
    .use((req, res, next) => {
        // on initialise cette liste si besoin
        if (typeof (req.session.todolist) == 'undefined') {
            req.session.todolist = [];
        }
        next();
    })

    .use(flash())

    // On affiche la route de base
    .get('/todo', function (req, res) {
        Todos.find({}, (err, todo) => {
            if (err) {
                console.log(err);
            }
            else {
                res.render('todo.pug', { todolist: todo, flash: req.flash() });
            }
        });
    })

    // On ajoute la todo indiquée dans le tableau todolist
    .post('/todo/ajouter', urlencodedParser, function (req, res) {
        if (req.body.newtodo != '' && req.body.category != 'Categories') {
            const todo = new Todos({ todo: req.body.newtodo, category: req.body.category });
            todo.save((err) => {
                if (err) {
                    console.log(err);
                } else {
                    req.flash('success', 'Todo ajouté avec succès');
                    res.redirect('/todo');
                }
            });
        }
    })

    // On supprime la todo selectionée dans la todolist
    .get('/todo/supprimer/:id', function (req, res) {
        let id = req.params.id;
        Todos.findByIdAndRemove(mongoose.Types.ObjectId(id), (err, todo) => {
            if (err) {
                console.log(err);
            }
            else {
                console.log(id);
                req.flash('delete', 'Todo supprimé avec succès');
                res.redirect('/todo');
            }
        });
    })

    // On supprime tous les todos de la todolist
    .get('/todo/supprimer', function (req, res) {
        Todos.deleteMany({}, (err) => {
            if (err) {
                console.log(err);
            }
            else {
                req.flash('deleteAll', 'Toutes les tâches ont été supprimées');
                console.log("Toutes les tâches ont été supprimées");
                res.redirect('/todo');
            }
        });
    })

    // On affiche le formulaire pour modifier un todo
    .get('/todo/modifier/:id/:category', function (req, res) {
        let id = req.params.id;
        console.log("Id get :", id);
        Todos.findById(id, (err, todo) => {
            if (err) {
                console.log(err);
            }
            else {
                res.render('modifier.pug', { todo: todo });
            }
        });
    })

    // On modifie le todo sélectionné 
    .post('/todo/modifier/', urlencodedParser, (req, res) => {
        let id = req.body.id;
        let modiftodo = req.body.modiftodo;
        let modifcategory = req.body.modifcategory;

        Todos.findByIdAndUpdate(id, { todo: modiftodo, category: modifcategory }, (err, updatedTodo) => {
            if (err) {
                console.log(err);
            }
            else {
                req.flash('modify', 'La tâche a bien été modifiée');
                res.redirect('/todo');
            }
        });
    })

    // Export en CSV
    .get('/todo/export', function (req, res) {
        Todos.find({}, (err, todos) => {
            if (err) {
                console.log(err);
            }
            else {
                const data = todos.map(todo => ({
                    nom: todo.todo,
                    categorie: todo.category
                }));

                csv.write(data, { headers: true }).pipe(ws);
                ws.on('finish', () => {
                    res.download('todos.csv');
                });
            }
        });
    })

    // On redirige vers la todolist si la page demandée n'est pas trouvée
    .use((req, res, next) => {
        res.redirect('/todo');
    })

    // On lance le serveur sur le port indiqué
    .listen(port, () => { console.log("Serveur à l'écoute sur le port %d", port); });