FROM node:14-alpine
WORKDIR /Users/mathieuboulot/Desktop/docker-node
    # "." correspond au workdir
COPY package*.json ./
    # "ci" installe uniquement les dépendences utiles au projet
RUN npm ci
COPY . .
EXPOSE 8080
CMD ["npm", "start"]