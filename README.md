## Procédure afin de lancer l'application docker

1) Modifiez le chemin dans **WORKDIR**

2) Lancez **docker compose up** et rendez-vous sur le port 8080 où l'application sera lancée.

PS : Pour ma part, je lance **Docker Desktop** pour que le docker compose fonctionne.